    package view.menue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;
	private User user;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user) {
		setBackground(Color.BLACK);
		this.alienDefenceController = alienDefenceController;
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;
		this.user = user;

		setLayout(new BorderLayout());

		
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		if (user == null) {
			JButton btnNewLevel = new JButton("Neues Level");
			btnNewLevel.setFont(new Font("Tahoma", Font.PLAIN, 11));
			btnNewLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnNewLevel_Clicked();
				}
			});
			pnlButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlButtons.add(btnNewLevel);

			JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
			btnUpdateLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnUpdateLevel_Clicked();
				}
			});
			pnlButtons.add(btnUpdateLevel);

			JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
			btnDeleteLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnDeleteLevel_Clicked();
				}
			});
			pnlButtons.add(btnDeleteLevel);
			
			JButton btnAbbrechen = new JButton("Abbrechen");
			btnAbbrechen.setFont(new Font("Tahoma", Font.PLAIN, 11));
			btnAbbrechen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					System.exit(0);
				}
			});
		} else {
			JButton btnTesten = new JButton("Testen");
			pnlButtons.add(btnTesten);
			btnTesten.addActionListener(new ActionListener() {
				;
				public void actionPerformed(ActionEvent e) {
					btnTesten_start();
						
					
				}
				
			});
			
		}
		
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.WHITE);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBackground(Color.BLACK);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	public void btnTesten_start() {
		Thread t = new Thread("GameThread") {

			@Override
			public void run() {
				
				List<Level> levels = alienDefenceController.getLevelController().readAllLevels();
				int level_id = Integer.parseInt((String)tblLevels.getModel().getValueAt(tblLevels.getSelectedRow(), 0));
				
				GameController gameController = alienDefenceController.startGame( levels.get(level_id - 1),user);
				new GameGUI(gameController).start();
				
			}
		};
		t.start();
	}
}

    
