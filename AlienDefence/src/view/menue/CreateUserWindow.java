package view.menue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.UserController;
import model.User;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

public class CreateUserWindow extends JFrame{
	private UserController controller;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	public CreateUserWindow(UserController controller) {
        this.controller = controller;
        
		getContentPane().setLayout(null);
		setSize(270,410);
		
		textField = new JTextField();
		textField.setBounds(145, 18, 100, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Vorname:");
		lblNewLabel.setBounds(10, 21, 139, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nachname:");
		lblNewLabel_1.setBounds(10, 46, 139, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Geburtsdatum:");
		lblNewLabel_2.setBounds(10, 71, 139, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Stra\u00DFe:");
		lblNewLabel_3.setBounds(10, 96, 139, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Hausnummer:");
		lblNewLabel_4.setBounds(10, 121, 139, 14);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Postleitzahl:");
		lblNewLabel_5.setBounds(10, 146, 139, 14);
		getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Stadt:");
		lblNewLabel_6.setBounds(10, 171, 139, 14);
		getContentPane().add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Login Name:");
		lblNewLabel_7.setBounds(10, 196, 139, 14);
		getContentPane().add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Passwort:");
		lblNewLabel_8.setBounds(10, 221, 139, 14);
		getContentPane().add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Einkommen:");
		lblNewLabel_9.setBounds(10, 246, 139, 14);
		getContentPane().add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Familienstand:");
		lblNewLabel_10.setBounds(10, 271, 139, 14);
		getContentPane().add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Abschlussnote:");
		lblNewLabel_11.setBounds(10, 296, 139, 14);
		getContentPane().add(lblNewLabel_11);
		
		textField_1 = new JTextField();
		textField_1.setBounds(145, 43, 100, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(145, 68, 100, 20);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(145, 93, 100, 20);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(145, 118, 100, 20);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(145, 143, 100, 20);
		getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(145, 168, 100, 20);
		getContentPane().add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(145, 193, 100, 20);
		getContentPane().add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(145, 218, 100, 20);
		getContentPane().add(textField_8);
		textField_8.setColumns(10);
		
		textField_9 = new JTextField();
		textField_9.setBounds(145, 243, 100, 20);
		getContentPane().add(textField_9);
		textField_9.setColumns(10);
		
		textField_10 = new JTextField();
		textField_10.setBounds(145, 268, 100, 20);
		getContentPane().add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(145, 293, 100, 20);
		getContentPane().add(textField_11);
		textField_11.setColumns(10);
		
		JButton btnNewButton = new JButton("Erstelle Nutzer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	            LocalDate localDate = LocalDate.parse(textField_2.getText(), formatter);
	            
	            User user = new User(
	                    -1, 
	                    textField.getText(), 
	                    textField_1.getText(), 
	                    localDate, 
	                    textField_3.getText(), 
	                    textField_4.getText(), 
	                    textField_5.getText(), 
	                    textField_6.getText(), 
	                    textField_7.getText(), 
	                    textField_8.getText(), 
	                    Integer.parseInt(textField_9.getText()), 
	                    textField.getText(), 
	                    Integer.parseInt(textField_10.getText())
	            );
	            controller.createUser(user);
	            System.out.println("Der Benutzer " + user.getLoginname() + " wurde erstellt.");
			}
		});
		btnNewButton.setBounds(10, 321, 235, 32);
		getContentPane().add(btnNewButton);
		
		JLabel lblLegenSieEinen = new JLabel("Legen Sie einen neuen Nutzer an");
		lblLegenSieEinen.setHorizontalAlignment(SwingConstants.LEFT);
		lblLegenSieEinen.setBounds(10, 0, 235, 14);
		getContentPane().add(lblLegenSieEinen);

	}
	
}