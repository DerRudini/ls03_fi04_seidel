package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * @author Chris Seidel
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance persistance) {
		this.userPersistance = persistance.getUserPersistance();
		}
	/**
	 * erstellt einen User in der Datenbank
	 * @param user User der erstellt wird
	 */
	public void createUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return userPersistance.readUser(username);
	}
	/**
	 * �ndert einen User in der Datenbank
	 * @param user User der ver�ndert wird
	 */
	public void changeUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.updateUser(user);
		}
	}
	/**
	 * l�scht einen User in der Datenbank falls dieser vorhanden ist
	 * @param user User der entfernt wird
	 */
	public void deleteUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.deleteUser(user);
		}
	}
	/**
	 * sieht nach ob beide Passw�rter gleich sind
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		if(user.getPassword().equals(passwort)) {
				return true;
		}
		return false;
	}
}
