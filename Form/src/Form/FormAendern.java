package Form;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FormAendern extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel background = new JPanel(null, true);
	private JLabel labelView = new JLabel();
	
	private JLabel taskOne = new JLabel();
	private JButton buttonRed = new JButton();
	private JButton buttonGreen = new JButton();
	private JButton buttonBlue = new JButton();
	private JButton buttonYellow = new JButton();
	private JButton buttonDefault = new JButton();
	private JButton buttonChooseColor = new JButton();
	
	private JLabel taskTwo = new JLabel();
	private JButton buttonArial = new JButton();
	private JButton buttonComicSans = new JButton();
	private JButton buttonCourier = new JButton();
	private JTextField textFieldInput = new JTextField();
	private JButton buttonChangeLabel = new JButton();
	private JButton buttonDeleteLabel = new JButton();
	
	private JLabel taskThree = new JLabel();
	private JButton buttonFontRed = new JButton();
	private JButton buttonFontBlue = new JButton();
	private JButton buttonFontBlack = new JButton();
	
	private JLabel taskFour = new JLabel();
	private JButton buttonAdd = new JButton();
	private JButton buttonSub = new JButton();
	
	private JLabel taskFive = new JLabel();
	private JButton buttonLeft = new JButton();
	private JButton buttonCenter = new JButton();
	private JButton buttonRight = new JButton();
	
	private JLabel taskSix = new JLabel();
	private JButton buttonExit = new JButton();

	public FormAendern(String title) {
		super(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int frameWidth = 400;
		int frameHeight = 600;
		this.setSize(frameWidth, frameHeight);
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int a = (dimension.width - (getSize()).width) / 2;
		int b = (dimension.height - (getSize()).height) / 2;
		
		this.setLocation(a, b);
		this.setResizable(false);
		
		Container pane = getContentPane();
		pane.setLayout((LayoutManager) null);
		this.background.setBounds(0, 0, 400, 600);
		pane.add(this.background);
		labelView.setFont(new Font("Arial", Font.PLAIN, 12));
		
		this.labelView.setBounds(8, 8, 379, 81);
		this.labelView.setText("Dieser Text soll verwerden.");
		this.labelView.setHorizontalAlignment(0);
		
		this.background.add(this.labelView);
		taskOne.setFont(new Font("Arial", Font.PLAIN, 12));
		
		this.taskOne.setBounds(8, 96, 371, 20);
		this.taskOne.setText("Aufgabe 1: Hintergrundfarbe");
		this.background.add(this.taskOne);
		buttonRed.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonRed.setBounds(8, 120, 115, 25);
		this.buttonRed.setText("Rot");
		this.buttonRed.setMargin(new Insets(2, 2, 2, 2));
		this.buttonRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_rot_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonRed);
		buttonGreen.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonGreen.setBounds(136, 120, 115, 25);
		this.buttonGreen.setText("Gr\u00FCn");
		this.buttonGreen.setMargin(new Insets(2, 2, 2, 2));
		this.buttonGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_gruen_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonGreen);
		buttonBlue.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonBlue.setBounds(264, 120, 115, 25);
		this.buttonBlue.setText("Blau");
		this.buttonBlue.setMargin(new Insets(2, 2, 2, 2));
		this.buttonBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_blau_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonBlue);
		buttonYellow.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonYellow.setBounds(8, 152, 115, 25);
		this.buttonYellow.setText("Gelb");
		this.buttonYellow.setMargin(new Insets(2, 2, 2, 2));
		this.buttonYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_gelb_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonYellow);
		buttonDefault.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonDefault.setBounds(136, 152, 115, 25);
		this.buttonDefault.setText("Standardfarbe");
		this.buttonDefault.setMargin(new Insets(2, 2, 2, 2));
		this.buttonDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_standard_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonDefault);
		buttonChooseColor.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonChooseColor.setBounds(264, 152, 115, 25);
		this.buttonChooseColor.setText("Farbe w\u00E4hlen");
		this.buttonChooseColor.setMargin(new Insets(2, 2, 2, 2));
		this.buttonChooseColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_farbe_auswaehlen_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonChooseColor);
		taskTwo.setFont(new Font("Arial", Font.PLAIN, 12));
		this.taskTwo.setBounds(8, 184, 371, 20);
		this.taskTwo.setText("Aufgabe 2: Text formatieren");
		this.background.add(this.taskTwo);
		buttonArial.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonArial.setBounds(8, 208, 115, 25);
		this.buttonArial.setText("Arial");
		this.buttonArial.setMargin(new Insets(2, 2, 2, 2));
		this.buttonArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_arial_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonArial);
		buttonComicSans.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonComicSans.setBounds(136, 208, 115, 25);
		this.buttonComicSans.setText("Comic Sans MS");
		this.buttonComicSans.setMargin(new Insets(2, 2, 2, 2));
		this.buttonComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_comic_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonComicSans);
		buttonCourier.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonCourier.setBounds(264, 208, 115, 25);
		this.buttonCourier.setText("Courier New");
		this.buttonCourier.setMargin(new Insets(2, 2, 2, 2));
		this.buttonCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_courier_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonCourier);
		textFieldInput.setFont(new Font("Arial", Font.PLAIN, 12));
		this.textFieldInput.setBounds(8, 240, 374, 20);
		this.textFieldInput.setText("Hier bitte Text eingeben");
		this.background.add(this.textFieldInput);
		buttonChangeLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonChangeLabel.setBounds(8, 264, 187, 25);
		this.buttonChangeLabel.setText("Ins Label schreiben");
		this.buttonChangeLabel.setMargin(new Insets(2, 2, 2, 2));
		this.buttonChangeLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_label_aendern_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonChangeLabel);
		buttonDeleteLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonDeleteLabel.setBounds(200, 264, 179, 25);
		this.buttonDeleteLabel.setText("Text im Label l\u00F6schen");
		this.buttonDeleteLabel.setMargin(new Insets(2, 2, 2, 2));
		this.buttonDeleteLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_label_loeschen_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonDeleteLabel);
		taskThree.setFont(new Font("Arial", Font.PLAIN, 12));
		this.taskThree.setBounds(8, 296, 371, 20);
		this.taskThree.setText("Aufgabe 3: Schriftfarbe \u00E4ndern");
		this.background.add(this.taskThree);
		buttonFontRed.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonFontRed.setBounds(8, 320, 115, 25);
		this.buttonFontRed.setText("Rot");
		this.buttonFontRed.setMargin(new Insets(2, 2, 2, 2));
		this.buttonFontRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_schrift_rot_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonFontRed);
		buttonFontBlue.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonFontBlue.setBounds(136, 320, 115, 25);
		this.buttonFontBlue.setText("Blau");
		this.buttonFontBlue.setMargin(new Insets(2, 2, 2, 2));
		this.buttonFontBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_schrift_blau_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonFontBlue);
		buttonFontBlack.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonFontBlack.setBounds(264, 320, 115, 25);
		this.buttonFontBlack.setText("Schwarz");
		this.buttonFontBlack.setMargin(new Insets(2, 2, 2, 2));
		this.buttonFontBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_schrift_schwarz_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonFontBlack);
		taskFour.setFont(new Font("Arial", Font.PLAIN, 12));
		this.taskFour.setBounds(8, 352, 371, 20);
		this.taskFour.setText("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		this.background.add(this.taskFour);
		buttonAdd.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonAdd.setBounds(8, 376, 187, 25);
		this.buttonAdd.setText("+");
		this.buttonAdd.setMargin(new Insets(2, 2, 2, 2));
		this.buttonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_plus_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonAdd);
		buttonSub.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonSub.setBounds(208, 376, 171, 25);
		this.buttonSub.setText("-");
		this.buttonSub.setMargin(new Insets(2, 2, 2, 2));
		this.buttonSub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_minus_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonSub);
		taskFive.setFont(new Font("Arial", Font.PLAIN, 12));
		this.taskFive.setBounds(8, 408, 371, 20);
		this.taskFive.setText("Aufgabe 5: Textausrichtung");
		this.background.add(this.taskFive);
		buttonLeft.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonLeft.setBounds(8, 432, 107, 25);
		this.buttonLeft.setText("linksb\u00FCndig");
		this.buttonLeft.setMargin(new Insets(2, 2, 2, 2));
		this.buttonLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_links_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonLeft);
		buttonCenter.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonCenter.setBounds(128, 432, 115, 25);
		this.buttonCenter.setText("zentriert");
		this.buttonCenter.setMargin(new Insets(2, 2, 2, 2));
		this.buttonCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_zentriert_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonCenter);
		buttonRight.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonRight.setBounds(256, 432, 123, 25);
		this.buttonRight.setText("rechtsb\u00FCndig");
		this.buttonRight.setMargin(new Insets(2, 2, 2, 2));
		this.buttonRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_rechts_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonRight);
		taskSix.setFont(new Font("Arial", Font.PLAIN, 12));
		this.taskSix.setBounds(8, 472, 371, 20);
		this.taskSix.setText("Aufgabe 6: Programm beenden");
		this.background.add(this.taskSix);
		buttonExit.setFont(new Font("Arial", Font.PLAIN, 12));
		this.buttonExit.setBounds(8, 496, 379, 73);
		this.buttonExit.setText("EXIT");
		this.buttonExit.setMargin(new Insets(2, 2, 2, 2));
		this.buttonExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FormAendern.this.btn_beenden_ActionPerformed(evt);
			}
		});
		this.background.add(this.buttonExit);
		setVisible(true);
	}

	public void btn_rot_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(Color.RED);
	}

	public void btn_gruen_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(Color.GREEN);
	}

	public void btn_blau_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(Color.BLUE);
	}

	public void btn_gelb_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(Color.YELLOW);
	}

	public void btn_standard_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(new Color(15658734));
	}

	public void btn_farbe_auswaehlen_ActionPerformed(ActionEvent evt) {
		this.background.setBackground(chooseColor());
	}

	public void btn_arial_ActionPerformed(ActionEvent evt) {
		this.labelView.setFont(new Font("Arial", 0, this.labelView.getFont().getSize()));
	}

	public void btn_comic_ActionPerformed(ActionEvent evt) {
		this.labelView.setFont(new Font("Comic Sans MS", 0, this.labelView.getFont().getSize()));
	}

	public void btn_courier_ActionPerformed(ActionEvent evt) {
		this.labelView.setFont(new Font("Courier New", 0, this.labelView.getFont().getSize()));
	}

	public void btn_label_aendern_ActionPerformed(ActionEvent evt) {
		this.labelView.setText(this.textFieldInput.getText());
	}

	public void btn_label_loeschen_ActionPerformed(ActionEvent evt) {
		this.labelView.setText("");
	}

	public void btn_schrift_rot_ActionPerformed(ActionEvent evt) {
		this.labelView.setForeground(Color.RED);
	}

	public void btn_schrift_blau_ActionPerformed(ActionEvent evt) {
		this.labelView.setForeground(Color.BLUE);
	}

	public void btn_schrift_schwarz_ActionPerformed(ActionEvent evt) {
		this.labelView.setForeground(Color.BLACK);
	}

	public void btn_plus_ActionPerformed(ActionEvent evt) {
		String aktuellerFont = this.labelView.getFont().getFontName();
		int groesse = this.labelView.getFont().getSize();
		this.labelView.setFont(new Font(aktuellerFont, 0, groesse + 1));
	}

	public void btn_minus_ActionPerformed(ActionEvent evt) {
		String aktuellerFont = this.labelView.getFont().getFontName();
		int groesse = this.labelView.getFont().getSize();
		if (groesse > 1)
			groesse--;
		this.labelView.setFont(new Font(aktuellerFont, 0, groesse));
	}

	public void btn_links_ActionPerformed(ActionEvent evt) {
		this.labelView.setHorizontalAlignment(2);
	}

	public void btn_zentriert_ActionPerformed(ActionEvent evt) {
		this.labelView.setHorizontalAlignment(0);
	}

	public void btn_rechts_ActionPerformed(ActionEvent evt) {
		this.labelView.setHorizontalAlignment(4);
	}

	public void btn_beenden_ActionPerformed(ActionEvent evt) {
		System.exit(1);
	}

	public Color chooseColor() {
		return JColorChooser.showDialog(this, "WFarbe", Color.white);
	}

	public static void main(String[] args) {
		new FormAendern("FormAendern");
	}
}